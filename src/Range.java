import java.util.ArrayList;
import java.util.List;

public class Range {
    private List<int[]> includes;
    Range(int[] originalRange){
        includes = new ArrayList<>();
        includes.add(originalRange);
    }
    void addInclusion(int[] inclusionRange){
        int i = 0;
        boolean added = false;
        while (!added && i < includes.size()){
            int[] range = includes.get(i);
            if(inclusionRange[0] < range[0]){
                if(inclusionRange[1] > range[0]){
                    if(inclusionRange[1] < range[1]) {
                        range[0] = inclusionRange[0];
                        includes.set(i, range);
                        added = true;
                    }else if(inclusionRange[1] > range[1]){
                        range[0] = inclusionRange[0];
                        range[1] = inclusionRange[1];
                        includes.set(i, range);
                        added = true;
                    }
                }else {
                    includes.add(inclusionRange);
                    added = true;
                }
            }else {
                if(inclusionRange[0] > range[1]){
                    includes.add(inclusionRange);
                    added = true;
                }else {
                    if(inclusionRange[1] > range[1]){
                        range[1] = inclusionRange[1];
                        includes.set(i, range);
                    }
                }
            }
            if(!added && inclusionRange[1] > range[1]){
                if(inclusionRange[0] < range[1]){
                    if(inclusionRange[0] > range[0]) {
                        range[1] = inclusionRange[1];
                        includes.set(i, range);
                        added = true;
                    }else {
                        range[1] = inclusionRange[1];
                        range[0] = inclusionRange[0];
                        includes.set(i, range);
                        added = true;
                    }
                }else {
                    includes.add(inclusionRange);
                    added = true;
                }
            }
            i++;
        }
    }


    void addExclusion(int[] exclusionRange){

        int i = 0;
        while (i < includes.size()){

            int[] range = includes.get(i);

            if(exclusionRange[0] > range[0]){
                if(exclusionRange[1] < range[1]){
                    includes.remove(i);
                    int[] left = new int[2];
                    int[] right = new int[2];
                    left[0] = range[0];
                    left[1] = exclusionRange[0]-1;
                    right[0] = exclusionRange[1]+1;
                    right[1] = range[1];
                    includes.add(left);
                    includes.add(right);
                }else {
                    if(exclusionRange[0] < range[1]) {
                        range[1] = exclusionRange[0] - 1;
                        includes.set(i, range);
                    }
                }
            }else if(exclusionRange[0] < range[0]){
                if(exclusionRange[1] < range[1]){
                    if(exclusionRange[1] > range[0]) {
                        range[0] = exclusionRange[1] + 1;
                        includes.set(i, range);
                    }
                }else {
                    includes.remove(i);
                }
            }
            i++;
        }
    }

    @Override
    public String toString(){
        int i = 0;
        StringBuilder line = new StringBuilder();
        while (i < includes.size()){
            int[] rangeHolder = includes.get(i);
            if(i < includes.size()-1) {
                line.append(rangeHolder[0]).append("-").append(rangeHolder[1]).append(",");
            }else {
                line.append(rangeHolder[0]).append("-").append(rangeHolder[1]);
            }

            i++;
        }
        return line.toString();
    }

}
