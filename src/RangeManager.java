import java.util.ArrayList;
import java.util.List;

public class RangeManager {

    private Range range;

    RangeManager(String includes, String excludes){

        String[] allIncludes = includes.split(", ");

        Range range = null;
        int i = 0;
        while (i < allIncludes.length){
            range = addRange(allIncludes[i], range);
            i++;
        }

        if(!excludes.equals("")) {
            String[] allExcludes = excludes.split(", ");
            i = 0;
            while (i < allExcludes.length) {
                        range = removeRange(allExcludes[i], range);
                i++;
            }
        }
        this.range = range;
    }

    private Range addRange(String line, Range range){
        String[] arrayIncludes = line.split("-");
        int[] includesArray = new int[2];
        includesArray[0] = Integer.parseInt(arrayIncludes[0]);
        includesArray[1] = Integer.parseInt(arrayIncludes[1]);

        if(range == null) {
            range = new Range(includesArray);
        }else {
            range.addInclusion(includesArray);
        }

        return range;
    }

    private Range removeRange(String line, Range range){
        String[] arrayIncludes = line.split("-");
        int[] excludesArray = new int[2];
        excludesArray[0] = Integer.parseInt(arrayIncludes[0]);
        excludesArray[1] = Integer.parseInt(arrayIncludes[1]);
        range.addExclusion(excludesArray);
        return range;
    }

    void writeOutput() {
        System.out.println(range.toString());
    }
}
