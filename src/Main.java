import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date date = new Date();
        System.out.println(sdf.format(date));

        //Scanner scanner = new Scanner(System. in);
        // String includes = scanner. nextLine();
        RangeGenerator rangeGenerator = new RangeGenerator();
        String includes = rangeGenerator.getIncludes(1000000);

        //Scanner scanner = new Scanner(System. in);
        //String excludes = scanner. nextLine();
        String excludes = rangeGenerator.getExcludes(1000000);
        RangeManager fixer = new RangeManager(includes,excludes);

        fixer.writeOutput();

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        date = new Date();
        System.out.println(sdf.format(date));


    }
}
