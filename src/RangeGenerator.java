import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class RangeGenerator {


        String getIncludes(int amount){

            StringBuilder list = new StringBuilder();
            Random random = new Random();
            int i = 0;
            while (i < amount){

                int one = i + random.nextInt(100);
                int two = one + random.nextInt(500)+(one+1);
                String line = null;
                if(i < amount-1) {
                    line = one + "-" + two + ", ";
                }
                else {
                    line = one + "-" + two;
                }
                if(line != null) {
                    list.append(line);
                }
                i++;
            }
            return list.toString();
        }

    String getExcludes(int amount){

        StringBuilder list = new StringBuilder();
        Random random = new Random();
        int i = 0;
        while (i < amount){

            int one = i + random.nextInt(50);
            int two = one + random.nextInt(300)+(one+1);
            String line;
            if(i < amount-1) {
                line = one + "-" + two + ", ";
            }
            else {
                line = one + "-" + two;
            }
            if(line != null) {
                list.append(line);
            }
            i++;
        }
        return list.toString();
    }



}
